# Welcome
---

Welcome to Makerstreet' Shared Gitlab CI YAML files documentation.

---
## Why?

Every Makerstreet project needs a specific set of jobs/steps necessary to ensure quality and automate our workflow. This project hosts all the common jobs our projects might use, reducing any duplication throughout our projects and keeping a base template from which one should start.

---
## Platforms

Each platforms has their own base YAML file, which builds on top of our [Common YAML File](common.md), you can find the specific details of the jobs those files contain here:

- [Python](platforms/python.md)