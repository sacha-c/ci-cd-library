# Python 

These are the jobs and rules for python builds.


## Jobs

### .do-dependency-check

Checks that both pyproject.toml and poetry.lock are correct and in sync. Override the `image` if you are using a different python version in your project (default `python 3.11`).

- Example:

```yaml
dependency-check:
  stage: test
  extends: 
    - .do-dependency-check 
    - .on-merge-request
```