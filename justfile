python_version := "python3.11"
mkdocs_version := "9.1.8"
venv := ".venv"
bin := venv + "/bin"

_docs-requirements:
    {{ python_version }} -m venv {{ venv }}
    {{ bin }}/pip install mkdocs-material=={{ mkdocs_version }}

docs: _docs-requirements
    {{ bin }}/mkdocs serve 